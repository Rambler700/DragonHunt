# -*- coding: utf-8 -*-
"""
Created on Tue Dec 19 13:48:52 2017

@author: enovi
"""

##License: GPL 3.0

## Dragon Hunt
import random
#values also reset in set_defaults
game_loop       = True
end_game        = False
days            = 1
copper_nuggets  = 0
copper_coins    = 100
start_health    = 100
start_magic     = 50
maxhealth       = start_health
maxmagic        = start_magic
health          = maxhealth
magic           = maxmagic
mining_skill    = 0
attack_skill    = 0
defense_skill   = 0
attack_bonus    = 0
defense_bonus   = 0
herbs           = 0
victory_points  = 0
capacity_level  = 0
promotion       = 0

def main():
    choice = 'y'
    print('***Welcome to Dragon Hunt!***')
    print('Explore the forest and the city!')
    print('Train your combat and magic skills and slay the dragon!')        
    while choice == 'y':               
        choice = input('New game y/n? ').lower()
        if choice == 'y':
            print ('Starting new game')
            set_defaults()
            dragonhunt()
        elif choice == 'n':
            return
        else:
            print('Enter a valid choice')
            choice = 'y'   
            
def set_defaults():
    global game_loop
    global end_game
    global days
    global copper_nuggets
    global copper_coins
    global start_health
    global start_magic
    global maxhealth
    global maxmagic     
    global health       
    global magic         
    global mining_skill  
    global attack_skill
    global defense_skill
    global attack_bonus
    global defense_bonus
    global herbs           
    global victory_points 
    global capacity_level
    global promotion    
    game_loop       = True
    end_game        = False
    days            = 1
    copper_nuggets  = 0
    copper_coins    = 100
    start_health    = 100
    start_magic     = 50
    maxhealth       = start_health
    maxmagic        = start_magic
    health          = maxhealth
    magic           = maxmagic
    mining_skill    = 0
    attack_skill    = 0
    defense_skill   = 0
    attack_bonus    = 0
    defense_bonus   = 0
    herbs           = 0
    victory_points  = 0
    capacity_level  = 0
    promotion       = 0            
            
def dragonhunt():
    global game_loop
    townchoice = ''
    global days
    while game_loop == True:
        print('It is day {} of the dragon hunt'.format(days))          
        townchoice = input('You can visit the (i)nn, (f)orest, (b)arracks, (m)ine, (t)ower, or (q)uit: ')
        if townchoice == 'i':
            visitinn()            
        if townchoice == 'f':
            visitforest()            
        if townchoice == 'b':
            visitbarracks()            
        if townchoice == 'm':
            visitmine()
        if townchoice == 't':
            visittower()
        if townchoice == 'q':
            game_loop = False
    
def visitinn():
    global days
    global copper_nuggets
    global copper_coins
    global health
    global magic
    global attack_skill
    global defense_skill
    global promotion
    global capacity_level
    global victory_points
    global herbs
    innchoice = ''
    room_cost = 20 + (promotion * 20)    
    print('The innkeeper welcomes you to the inn')
    print('A drink costs 10 coins, a room costs {} coins per night'.format(room_cost))
    print('Buying copper nuggets for 5 coins each')
    innchoice = input('Would you like to (r)est, have a (d)rink, sell (n)uggets, see (c)haracter, or (l)eave? ')
    if innchoice == 'r':
        if copper_coins >= room_cost:
            print('You rest for the night')
            copper_coins -= room_cost
            health = maxhealth
            magic = maxmagic
            days += 1
        else:
            print('You cannot afford to rent a room')
        print('Health is: ',health, 'Magic is: ',magic)    
        
    if innchoice == 'd':
        if copper_coins >= 10:        
            print('You sit down and have a drink')
            copper_coins -= 10
            if defense_skill < 3:
                defense_skill += 1
                print('You feel better nourished.')
            gossip()
        else:
            print('You cannot afford a drink')
        
    if innchoice == 'n':
        print('You sell your copper nuggets to the innkeeper' )
        copper_coins += (copper_nuggets * 4)
        copper_nuggets = 0
        print('You now have {} copper coins.'.format(copper_coins))
        
    if innchoice == 'c':
        print('Health is: ',health)
        print('Magic is: ',magic)
        print('You have {} coins'.format(copper_coins))
        print('Attack skill is: ',attack_skill)
        print('Defense skill is: ',defense_skill)
        print('Magic skill is: ',capacity_level)
        print('Battles won is: ',victory_points)
        if promotion == 0:
            print('Military rank is Recruit')
        if promotion == 1:
            print('Military rank is Squire')
        if promotion == 2:
            print('Military rank is Lieutenant')
        if promotion == 3:
            print('Military rank is Captain')
        if promotion == 4:
            print('Military rank is General')            
        print('You have {} herbs'.format(herbs))
        
    if innchoice == 'l':
        print('You leave the inn')
    
def visitforest():
    global days
    global end_game
    forestchoice = 'y'
    # It takes a day to travel between the town and the forest
    days += 1
    print('The forest is vast and wild.')
    while forestchoice == 'y':
        print('You walk further into the forest.')
        encounter()
        if end_game == True:
            return
        forestchoice = input('Continue into the forest y/n? ')
    days += 1    
    print('You leave the forest and return to town.')

def visitbarracks():
    global copper_coins
    global attack_skill
    global promotion
    global maxhealth
    attackq = ''
    training_cost = 0
    training_cost = 100 + (100 * attack_skill)
    
    print('Soldiers exercise inside the barracks.')
    
    if attack_skill <= 10 + (10 * promotion):
        if copper_coins >= training_cost:
            attackq = input('Would you like attack training for {} coins? y/n '.format(training_cost))
            if attackq == 'y':
                copper_coins -= training_cost
                attack_skill += 1
                print('You have {} copper coins left.'.format(copper_coins))
        else:
            print('You cannot afford to train right now.')   
    
    if victory_points > 30:
        if promotion == 0:
            print('You have been promoted to Squire! ')
            promotion = 1
            maxhealth += 20
            
    if victory_points > 75:
        if promotion == 1:
            print('You have been promoted to Lieutenant! ')
            promotion = 2
            maxhealth += 20
            
    if victory_points > 150:
        if promotion == 2:
            print('You have been promoted to Captain! ')
            promotion = 3
            maxhealth += 20
    
    if victory_points > 300:
        if promotion == 3:
            print('You have been promoted to General! ')
            promotion = 4
            maxhealth += 20        
    
def visitmine():
    minechoice = ''
    a          = 0
    global copper_nuggets
    global days
    global mining_skill
    mined = 0
    print('You enter the town copper mine.')
    print('You see a rack filled with rusty picks.')
    minechoice = input('Would you like to mine for copper? y/n: ')
    if minechoice == 'y':
        mined = 5 + mining_skill
        copper_nuggets += random.randint(0,mined)
        days += 1
        print('You have {} copper nuggets.'.format(copper_nuggets))
        #make mining skill rise more slowly
        a = random.randint(0,4)
        if a == 1:
            mining_skill += 1
        print('Mining skill is: ', mining_skill)
    else:
        print('You leave the town copper mine.')
        
def visittower():
    global copper_coins
    global capacity_level
    global maxmagic
    global promotion
    global days
    choice = ''
    magic_price = 0
    magic_price = 100 + (100 * capacity_level)
    print('You enter the tower and see magicians practicing their spells.')
    # Link maximum magic skill to combat rank (and encounter strength)
    if capacity_level <= 5 + (5 * promotion):
        print('Magic training will cost {} coins.'.format(magic_price))    
        if copper_coins >= magic_price:
            choice = input('Would you like to train your magic skills y/n?')
            if choice == 'y':
                print('You train your magic skills today.')
                maxmagic += 5
                copper_coins -= magic_price    
                capacity_level += 1
                days += 1
    print('You leave the tower.')
        
def gossip():
    global herbs
    choice = 0
    choice = random.randint(0,14)
    if choice == 0:
        print('The barracks rewards experienced fighters')
    if choice == 1:
        print('The inn also restores magical energy')
    if choice == 2:
        print('Save herbs for tougher enemies')
    if choice == 3:
        print('Miners frequently visit the inn')
    if choice == 4:
        print('Miners are strong fighters')
    if choice == 5:
        print('It takes a day to travel to the forest')
    if choice == 6:
        print('Weaker animals fear skilled officers.')
    if choice == 7:
        print('Combat training rapidly increases in cost')
    if choice == 8:
        print('Healing spells can win a tough battle')
    if choice == 9:
        print('A fellow adventurer gives you a medical herb')
        herbs += 1 
    if choice == 10:
        print('Magic training increases spell cost')
    if choice == 11:
        print('Many bears live in the forest')
    if choice == 12:
        print('Visit the tower after a promotion')
    if choice == 13:
        print('Become a General to challenge the dragon')
    if choice == 14:
        print('Medical herbs can be used along with healing magic')
    if choice == 15:
        print('Officers quarters are more expensive')
    if choice == 16:
        print('Training is available for higher military ranks')
        
def encounter():
    global maxhealth
    global health
    global maxmagic
    global magic
    global herbs
    global copper_nuggets
    global copper_coins
    event = random.randint(0,15)
    if event == 0:
        print('The tree leaves rustle but you see no creatures nearby.')
    if event == 1:
        print('Sunshine seeps through the forest canopy.')
    if event == 2:    
        print('You see a blackberry bush with ripe berries.')
        if health < maxhealth:
            health += 10
    if event == 3:    
        print('You drink water from a rapidly flowing stream.')
        if health < maxhealth:
            health += 5
        if magic < maxmagic:
            magic += 5
    if event == 4:
        print('You find some medicinal herbs.')
        herbs += 1
    if event == 5:
        print('Enemy attack!')
        combat()
    if event == 6:
        print('Enemy attack!')
        combat()
    if event == 7:
        print('You find copper nuggets lying beneath some leaves.')
        copper_nuggets += random.randint(1,5)
    if event == 8:
        print('You are injured by a sharp rock.')
        health -= 5
    if event == 9:
        print('Enemy attack!')   
        combat()
    if event == 10:
        print('You find some coins lying on the ground.')
        copper_coins += random.randint(1,10)
    if event == 11:
        print('You see an apple tree.')
        if health < maxhealth:
            health += 5
    if event == 12:
        print('You find a hidden treasure chest.')
        copper_coins += random.randint(1,20)
        herbs += 1
    if event == 13:
        print('You see an old oak tree.')
    if event == 14:
        print('Enemy attack!')   
        combat()  
    if event == 15:
        print('You smell smoke from burning trees.')
        
def combat():
    global maxhealth
    global health
    global maxmagic
    global magic
    global capacity_level
    global mining_skill
    global copper_coins
    global attack_skill
    global defense_skill
    global promotion
    global herbs
    global victory_points
    global game_loop
    global end_game
    choice             = ''
    choice2            = ''
    choice3            = ''
    selection          = 0
    enemy_health       = 100
    enemy_name         = ''
    reward             = 0
    attack_bonus       = 0
    defense_bonus      = 0
    enemy_attack_bonus = 0
    c                  = 0
    d                  = 0
    e                  = 0
    magic_power        = 0
    
    magic_power        = 5 + (capacity_level / 5)
    
    attack_bonus       += (mining_skill / 5)
    attack_bonus       += (attack_skill / 2)
    
    defense_bonus      += (defense_skill / 2)
    
    selection = random.randint(0,5)
    selection += (promotion * 3)
    
    if selection == 0:
        enemy_name = 'Fox'
        enemy_health = 60
        reward = 2
    if selection == 1:
        enemy_name = 'Coyote'
        enemy_health = 70
        reward = 3
    if selection == 2:
        enemy_name = 'Cougar'
        enemy_health = 80
        reward = 5
    if selection == 3:
        enemy_name = 'Wolf'
        enemy_health = 90
        reward = 8
    if selection == 4:
        enemy_name = 'Bull'
        enemy_health = 110
        enemy_attack_bonus = 5
        reward = 10
    if selection == 5:
        enemy_name = 'Black Bear'
        enemy_health = 120
        enemy_attack_bonus = 5
        reward = 15
    if selection == 6:
        enemy_name = 'Brown Bear'
        enemy_health = 130
        enemy_attack_bonus = 10
        reward = 30
    if selection == 7:
        enemy_name = 'Grizzly Bear'
        enemy_health = 180
        enemy_attack_bonus = 15
        reward = 35
    if selection == 8:
        enemy_name = 'Dire Wolf'
        enemy_health = 160
        enemy_attack_bonus = 20
        reward = 40    
    if selection == 9:
        enemy_name = 'Polar Bear'
        enemy_health = 200
        enemy_attack_bonus = 20
        reward = 60
    if selection == 10:
        enemy_name = 'Panther'
        enemy_health = 190
        enemy_attack_bonus = 25
        reward = 65        
    if selection == 11:
        enemy_name = 'Tiger'
        enemy_health = 180
        enemy_attack_bonus = 30
        reward = 70   
    if selection == 12:
        enemy_name = 'Lion'
        enemy_health = 220
        enemy_attack_bonus = 30
        reward = 80
    if selection == 13:
        enemy_name = 'Hippopotamus'
        enemy_health = 250
        enemy_attack_bonus = 35
        reward = 125     
    if selection == 14:
        enemy_name = 'Gorilla'
        enemy_health = 280
        enemy_attack_bonus = 35
        reward = 120        
    if selection == 15:
        enemy_name = 'Buffalo'
        enemy_health = 240
        enemy_attack_bonus = 40
        reward = 125
    if selection == 16:
        enemy_name = 'Elephant'
        enemy_health = 300
        enemy_attack_bonus = 40
        reward = 130       
    if selection == 17:
        enemy_name = 'Dragon'
        enemy_health = 500
        enemy_attack_bonus = 50
        reward = 300        
    
    print('Entering combat with: ',enemy_name)
    while enemy_health > 0:
        if herbs > 0:
            choice = input('Use medicinal herbs y/n? ')
            if choice == 'y':
                print('Used medicinal herbs')
                herbs -= 1
                health += random.randint(10,30)
        if magic >= (magic_power):
            choice2 = input('Use magic y/n? ')
            if choice2 == 'y':
                if magic >= (magic_power):
                    choice3 = input('Enhance (s)hield or (w)eapon or cast direct (h)eal or (d)amage spell: ')
                    if choice3 == 'h':
                        print('Casting Heal')
                        magic -= magic_power
                        health += 10 + magic_power * random.randint(0,6)
                    if choice3 == 'w':
                        print('Enhancing Weapon')
                        magic -= magic_power
                        attack_bonus += magic_power / 2
                    if choice3 == 's':
                        print('Enhancing Shield')
                        magic -= magic_power
                        defense_bonus += magic_power / 2
                    if choice3 == 'd':
                        print('Casting Damage')
                        magic -= magic_power
                        enemy_health -= 10 + magic_power * random.randint(0,6)
        #prevent infinite health gains                
        if health > maxhealth:
            health = maxhealth
        if magic > maxmagic:
            magic = maxmagic                
        
        your_attack = random.randint(0,30)
        enemy_attack = random.randint(0,30)
        
        c = your_attack + attack_bonus
        if c > 0:
            enemy_health -= c
        print('You hit the {}'.format(enemy_name))    
        d = enemy_attack - defense_bonus + enemy_attack_bonus
        if d > 0:
            health -= d
        print('The {} hits you'.format(enemy_name))    
            
        #cast variables to clean up output
        health = int(health)
        magic = int(magic)
        enemy_health = int(enemy_health)
        
        print('Health remaining: ',health)
        print('Magic remaining:  ',magic)
        print(enemy_name,'Health Remaining: ',enemy_health)
        
        if health <= 0:
            print('You were defeated')
            game_loop = False
            end_game = True
            return
        if enemy_health <= 0:
            print('You defeated the enemy')
            print('Reward is: ',reward)
            copper_coins += reward
            #changed from 1 to random gain for balancing reasons
            e = random.randint(0,4)
            if e == 0:
                attack_skill += 1
            if e == 1:
                defense_skill += 1
            if e == 2:    
                maxhealth += 1
            if e == 3:    
                maxmagic += 1
            victory_points += 1
            if enemy_name == 'Dragon':
                victory()
            
def victory():
    global days
    global game_loop
    global end_game
    print('You defeated the Dragon!')
    print('Your quest took {} days'.format(days))            
    game_loop = False
    end_game = True
    return

main()    
    